<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, user-scalable=no"/>
<meta name="format-detection" content="telephone=no">
<!-- site info -->
<meta name="author" content="www.tripi.com.ar"/>
<meta name="keywords" content=""/>
<meta name="description" content=""/> 
<meta name="alt-tags" content=""/>
<meta name="robots" content="index, archive, follow"/>
<!-- facebook metas -->
<meta property="og:image" content="images/fb_thumb.jpg"/>
<meta property="og:title" content=""/>
<meta property="og:description"content=""/>
<!-- fav & smartphone icons -->
<link rel="apple-touch-icon" sizes="57x57" href="assets/ico/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/ico/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/ico/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/ico/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/ico/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/ico/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/ico/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/ico/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/ico/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="assets/ico/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="assets/ico/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="assets/ico/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="assets/ico/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="assets/ico/manifest.json">
<link rel="mask-icon" href="assets/ico/safari-pinned-tab.svg" color="#ea3267">
<link rel="shortcut icon" href="assets/ico/favicon.ico">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="assets/ico/mstile-144x144.png">
<meta name="msapplication-config" content="assets/ico/browserconfig.xml">
<meta name="theme-color" content="#ea3267">
<!-- jquery -->
<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.easing.1.3.min.js"></script>
<!-- font-awesome -->
<link rel="stylesheet" href="assets/css/font-awesome.css" type="text/css"/>
<!-- jquery.scrollTo -->
<script type="text/javascript" src="assets/js/jquery.scrollTo.min.js"></script>
<!-- owl.carousel -->
<link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css"/>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<!-- fancyBox -->
<script type="text/javascript" src="assets/js/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox.css" media="screen"/>
<!-- onepagenav -->
<script src="assets/js/jquery.nav.js"></script>
<!-- bootstrap -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"/>
<!-- main -->
<link rel="stylesheet" href="assets/css/normalize.css" type="text/css"/>
<link rel="stylesheet" href="assets/css/main.css" type="text/css"/>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->