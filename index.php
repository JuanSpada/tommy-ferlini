<!DOCTYPE html>
<html lang="es">
 <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>Tommy Ferlini: DJ</title>
	<!-- Google ReCaptcha -->
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <?php include('inc/i_head.php'); ?>
</head>
<body>
  
	<?php include('inc/i_header.php'); ?>

	<div class="wrapper">

		<section id="hero">
			<div class="hero-logo"></div>
			<div class="section-header section-header1"></div>
		</section>

		<section id="social-media" class="padding-v-large">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="widget margin-large-b">
							<iframe width="100%" height="640" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https://soundcloud.com/tommyferlini&amp;auto_play=false"></iframe>
						</div>
					</div>
					<div class="col-md-4 hidden-xs">
						<div class="row">
							<div class="col-md-12 col-sm-6">
								<div class="feed feed-fb margin-large-b">
									<div id="fb-root"></div>
									<script>(function(d, s, id) {
									  var js, fjs = d.getElementsByTagName(s)[0];
									  if (d.getElementById(id)) return;
									  js = d.createElement(s); js.id = id;
									  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5";
									  fjs.parentNode.insertBefore(js, fjs);
									}(document, 'script', 'facebook-jssdk'));</script>
									<div class="fb-page" data-href="http://www.facebook.com/tommyferlini" data-tabs="timeline" data-height="300px" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="false">
										<div class="fb-xfbml-parse-ignore">
											<blockquote cite="http://www.facebook.com/tommyferlini">
												<a href="http://www.facebook.com/tommyferlini">Tommy Ferlini</a>
											</blockquote>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-6">
								<div class="feed feed-tw">
									<a class="twitter-timeline"  href="https://twitter.com/TommyFerlini" data-widget-id="703255644414017537">Tweets por el @TommyFerlini.</a>
									<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="bio">
			<div class="section-header section-header2">
				<div class="container">
					<h1 class="section-title color-w">BIO</h1>
				</div>
			</div>
			<div class="padding-v-xlargesss">
				<div class="container">
					<div class="bg white padding-full-large">
						<div class="row">
							<div class="col-md-4">
								<p class="margin-large-b">Nacido en Buenos Aires en el a�o 1994, fue a los 11 a�os cuando comenz� a notar que la m�sica era un de sus verdaderas pasiones. Con solo 12, arranco musicalizando primero muy peque�os eventos entre amigos y compa�eros del colegio. R�pidamente empez� a escalar cada vez mas, realizando todo tipo de eventos privados como cumplea�os, fiestas de 50, civiles, etc.</p>
							</div>
							<div class="col-md-4">
								<p class="margin-large-b">Con solo 15 a�os logro su primera residencia en uno de los clubs mas exclusivos de Punta Del Este, Ocean Club (edici�n Matinee), iniciando ah� su carrera en los club's mas importantes de Buenos Aires, Mar del Plata y Punta Del Este. Roxy, Tunnel, Caix, Club Caix, Loka, Espacio Quintana, La Rural, Pacha, Jet, Pink, Godoy San Isidro, Tribuna Plaza (hip�dromo de Palermo), Piratas, Bali, Samsara Beach, La Caseta, son solo algunos de los lugares en donde el joven dj musicalizo distintas fiestas y eventos sociales.</p>
							</div>
							<div class="col-md-4">
								<p class="margin-large-b">En la actualidad, tommy ferlini es el dj Residente del exitos�simo turno de los viernes en JET LOUNGE, llamado JET ROCK del cual tambi�n es organizador y productor. Su estilo varia desde el house hasta el funny music, mezclando sonidos nuevos con hits retro logra su combinaci�n perfecta para hacer bailar a todos y a todas!</p>
							</div>
						</div>
					</div>
					<img src="assets/images/bio_bg.jpg" class="full margin-xlarge-b">
				</div>
			</div>
		</section>

		<section id="tour-dates">
			<div class="section-header section-header3">
				<div class="container">
					<h1 class="section-title color-w">TOUR DATES</h1>
				</div>
			</div>
			<div class="has-bg bg graylight">
				<div class="container">
					<div class="bg white padding-full-large margin-xlarge-b">
						<table class="table table-striped">
							<thead>
								<tr>
									<td>DATE</td>
									<td>PARTY</td>
									<td>PLACE</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>01/12</td>
									<td>"TASCANI NIGHT" By JET ROCK</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>02/12</td>
									<td>"EL NILO" AKA ANA PAULA MUSIC</td>
									<td>@ROSEINRIO</td>
								</tr>
								<tr>
									<td>08/12/2017</td>
									<td>"PRIVATE POOL PARTY"</td>
									<td>@TIGRE</td>
								</tr>
								<tr>
									<td>08/12/2017</td>
									<td>"PH.BATTLE" By JET ROCK</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>09/12/2017</td>
									<td>"EL NILO" AKA ANA PAULA MUSIC #AFTERCAROLA</td>
									<td>@ROSEINRIO</td>
								</tr>
								<tr>
									<td>12/12/2017</td>
									<td>#LOREALCLOSINGYEAR</td>
									<td>@UPTOWN</td>
								</tr>
								<tr>
									<td>15/12/2017</td>
									<td>#MYSTERIUM By JET ROCK</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>16/12/2017</td>
									<td>"EL NILO" AKA ANA PAULA MUSIC</td>
									<td>@ROSEINRIO</td>
								</tr>
								<tr>
									<td>22/12/2017</td>
									<td>#AMAZONIA "POOL & PARTY"</td>
									<td>@ROSEINRIO</td>
								</tr>
								<tr>
									<td>22/12/2017</td>
									<td>#CLOSING FIESTA By JET ROCK</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>23/12/2017</td>
									<td>#CLOSINGNILO ANA PAULA</td>
									<td>@ROSEINRIO</td>
								</tr>
								<tr>
									<td>29/12/2017</td>
									<td>"CASINO PUNTA DEL ESTE" </td>
									<td>@MANTRA</td>
								</tr>
								<tr>
									<td>31/12/2017</td>
									<td>"NYE JET BA At PUNTA DEL ESTE</td>
									<td>@BAGATELLE</td>
								</tr>
								<tr>
									<td>26/01/2018</td>
									<td>"SUMMER EDITION" By JET ROCK</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>27/01/2018</td>
									<td>" EXODUS " PUNTA DEL ESTE</td>
									<td>@THEMANSION</td>
								</tr>
								<tr>
									<td>02/02/2018</td>
									<td>"SUMMER EDITION" By JET ROCK"</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>09/02/2018</td>
									<td>"SUMMER EDITION" By JET ROCK"</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>16/02/0208</td>
									<td>"SUMMER EDITION" By JET ROCK"</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>23/02/2018</td>
									<td>"SUMMER EDITION" By JET ROCK"</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>02/03/2018</td>
									<td>"SUMMER EDITION" By JET ROCK"</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>09/03/2018</td>
									<td>"SUMMER EDITION" By JET ROCK"</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>16/03/2018</td>
									<td>"SUMMER EDITION" By JET ROCK"</td>
									<td>@JETBA</td>
								</tr>
								<tr>
									<td>17/03/2018</td>
									<td>"DADDY YANKEE & OZUNA"</td>
									<td>@GEBA</td>
								</tr>
								<tr>
									<td>23/03/2018</td>
									<td>"A NIGHT IN DUBLIN" By JET ROCK"</td>
									<td>@JETBA</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>

		<section id="press">
			<div class="section-header section-header4">
				<div class="container">
					<h1 class="section-title color-w">PRESS</h1>
				</div>
			</div>
			<div class="bg white padding-v-xlarge">
				<div class="container">
					<h2 class="margin-b">PRESS KIT</h2>
					<hr class="margin-b">
					<a href="files/press-kit.zip" class="download-press-kit margin-b" target="_blank">Hac� click para descargar el press kit</a>
					<hr class="margin-xlarge-b">
					<div class="row">
						<?php for ($i=1; $i < 21; $i++) { ?>
						<div class="col-sm-4 col-md-3">
							<a href="images/press/thumb<?php echo $i; ?>.jpg" class="fancybox-image thumb margin-large-b" rel="gallery_place" style="background:url('images/press/thumb<?php echo $i; ?>.jpg') center center no-repeat;">
								<div class="overlay"></div>
							</a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>

		<section id="contacto">
			<div class="section-header section-header5">
				<div class="container">
					<h1 class="section-title color-w">CONTACT</h1>
				</div>
			</div>
			<div class="container">
				<div class="bg white padding-full-large margin-large-b">
					<div class="row">
						<div class="col-sm-6">
							<p>
								Booking &amp; Management<br/>
								<a href="mailto:info@tommyferlini.com" class="color-1">info@tommyferlini.com</a>
							</p>
							<hr class="margin-large-t margin-large-b">
							<img src="assets/images/contacto_logo.svg" class="hidden-xs" style="display: block; margin: 0 auto; width: 100%; max-width: 100%;">
						</div>
						<div class="col-sm-6">
							<form action="contact.php" name="frmContact" method="POST" id="frmContact" class="margin-large-b-xs">
								<div class="form-group">
									<label for="" class="label-control">Nombre</label>
									<input type="text" name="name" class="form-control" required placeholder="Nombre *">
								</div>
								<div class="form-group">
									<label for="" class="label-control">E-mail</label>
									<input type="text" name="mail" class="form-control" required placeholder="E-mail *">
								</div>
								<div class="form-group">
									<label for="" class="label-control">Teléfono</label>
									<input type="text" name="phone" class="form-control" required placeholder="Teléfono">
								</div>
								<div class="form-group">
									<label for="" class="label-control">Mensaje</label>
									<textarea class="form-control" name="message" required placeholder="Comentarios" style="height:100px;"></textarea>
								</div>
								<div class="g-recaptcha m-auto" data-sitekey="6LeO7dgUAAAAAPU3t3DJtXr1snG6LL57SX39GU4l"></div>
                                    <div class="col-md-12 mt-3 text-center">
                                        <div id="g-recaptcha-error" class="mb-2"></div>
										<button type="submit" class="btn btn-primary pull-right">Enviar</button>
                                    </div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		
	</div>

	<?php include('inc/i_footer.php'); ?>

	<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
	<!-- Sweet Alert -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	
	<!-- Contact from script -->
	<script src="assets/js/contact.js"></script>
	
	<!-- <script type="text/javascript" src="assets/js/jquery.validate.js"></script> -->

	<!-- <script language="JavaScript">
		$(document).ready(function () {
			
			jQuery.validator.messages.required = "";
					
		    $('#frmContact').validate({
		        rules: {
		        	name: {
		        		required: true,
		                minlength: 2
		            },
		            mail: {
		        		email: true
		            }
		        },
		        messages: {
		        	name: {
		                required: "<i class='fa fa-times'></i>&nbsp;Nombre y Apellido son requeridos",
		                minlength: "<i class='fa fa-times'></i>&nbsp;Minimo 2 caracteres"
		            },
		            mail: {
		                required: "<i class='fa fa-times'></i>&nbsp;E-mail es requerido",
		                email: "<i class='fa fa-times'></i>&nbsp;E-mail es invalido"
		            }
		        },
		        errorPlacement: function (error, element) {
		            element.focus(function () {
		                $("#message_response").html(error);
		            }).blur(function () {
		                $("#message_response").html('<i class="fa fa-times"></i>&nbsp;Datos incompletos. Completa, por favor, los campos en blanco.');
		            });
		        },
		        submitHandler: function(form) {
					$.ajax({
						cache: false,
						type: 'POST',
						data: $("#frmContact").serialize(),
						url : 'process/contact.php',
						success: function (html) {
							if(html == 1) {
								$("#message_response").html('<i class="fa fa-check"></i>&nbsp;Mensaje enviado con exito. Muchas gracias.');
								$("#submit_contact").hide();
							} else {
								$("#message_response").html('<i class="fa fa-times"></i>&nbsp;'+html);
							}
						},
						error: function(data, textStatus, jqXHR) {
							alert('error')
						}
					});
				}
		    });
		});
	</script> -->

</body>
</html>