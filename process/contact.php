<?php
	
		$PathtoRoot = '../';
		
		//--------------------------------------------------------------------------------------------------------//
		//MAILER
		
		require($PathtoRoot . "lib/mailer/class.phpmailer.php");
		
		$email = new PHPMailer();
		
		$nombre_envio = 'Tommy Ferlini';
		//$direccion_envio = 'mariano.tripicchio@tripi.com.ar';
		$direccion_envio = 'info@tommyferlini.com';
	    
	    //--------------------------------------------------------------------------------------------------------//
	    //CONTENIDO
	    
	    $subject = "Tommy Ferlini: Contacto de ". $_POST['name'];
	
		$message = 'Nombre: '. $_POST['name'] . "<br />";
		$message .= 'Telefono: '. $_POST['phone'] . "<br />";
		$message .= 'E-mail: '. $_POST['mail'] . "<br />";
		$message .= 'Mensaje: '. $_POST['message'] . "<br />";
	    
				
		$email->From = 'info@tommyferlini.com';
		$email->FromName = $nombre_envio;

		$email->Subject = $subject;
		$email->AddReplyTo($_POST['mail'], $_POST['name']);
		$email->AddAddress($direccion_envio, $nombre_envio);		
		
		$email->AddBCC( "tripi@me.com", "TRIPI");
		
		$email->IsHTML(true); 
	
		$email->Body = $message;
		
		if(!$email->Send()) {
		   echo('Error al enviar el contacto.');
		   exit;
		} else {
			echo(1);
		}

		
?>