<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


$url = "https://hooks.zapier.com/hooks/catch/803715/odrkp0g/";

$data = array(
    'name' => $_POST['name'],
    'mail' => $_POST['mail'],
    'phone' => $_POST['phone'],
    'message' => $_POST['message'],
);

$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) { }