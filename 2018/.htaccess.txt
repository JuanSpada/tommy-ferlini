RewriteEngine on
RewriteCond %{HTTP_HOST} ^example.com [NC]
RewriteRule ^(.*)$ http://www.example.com/$1 [L,R=301,NC]

AddType video/mp4  mp4 m4v
AddType audio/mp4  m4a
AddType video/ogg  ogv
AddType audio/ogg  ogg oga
AddType video/webm webm