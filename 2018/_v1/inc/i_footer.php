<footer>
	<div class="bg black padding-v-large">
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-md-6">
					<ul class="social">
						<li><a href="http://www.facebook.com/tommyferlini" target="_blank"><i class="fa fa-facebook fa-fw"></i></a></li>
						<li><a href="http://twitter.com/tommyferlini" target="_blank"><i class="fa fa-twitter fa-fw"></i></a></li>
						<li><a href="http://www.instagram.com/tommyferlini/" target="_blank"><i class="fa fa-instagram fa-fw"></i></a></li>
						<li><a href="http://soundcloud.com/tommy-ferlini" target="_blank"><i class="fa fa-soundcloud fa-fw"></i></a></li>
					</ul>
				</div>
				<div class="col-sm-8 col-md-6 hidden-xs">
					<?php include('inc/i_nav.php'); ?>
				</div>
			</div>
			<hr class="white"/>
			<div class="row">
				<div class="col-sm-6">
					<p class="copy color-w margin-t">&copy; Copyright 2016 &mdash; Tommy Ferlini</p>
				</div>
				<div class="col-sm-6">
					<a href="http://www.tripi.com.ar" class="logo-tripi margin-t pull-right" target="_blank"></a>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</footer>

<script src="assets/js/bootstrap.min.js"></script>

<script type="text/javascript">
	
	$(function(){

		$('.fancybox-image').fancybox({
			openEffect  : 'fade',
			closeEffect : 'fade'
		});

		$('#nav-main a').click(function(){
			$('#nav-main').collapse('hide');
		});

		$('.navbar-nav').onePageNav({
			scrollOffset: 80
		});

	});

</script>