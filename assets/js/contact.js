var form = document.querySelector('#frmContact');

form.addEventListener('submit', function(event) {
    event.preventDefault();
    var response = grecaptcha.getResponse();
    if(response.length == 0) {
        document.getElementById('g-recaptcha-error').innerHTML = '<span style="color:red;">Completa este campo.</span>';
        return false;
    }else{
        document.getElementById('g-recaptcha-error').innerHTML = '';
        Swal.fire(
            '¡Enviado correctamente!',
            '¡Gracias por escribirnos, nos pondremos en contacto a brevedad!',
            'success'
            )
            let formData = new FormData(form);
            form.reset();
            fetch("contact.php",
            {
                body: formData,
                method: "post"
            }
        );
    }
});